package com.example.service1.dao;

import com.example.service1.entity.TbUserDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (TbUser)表数据库访问层
 *
 * @author makejava
 * @since 2019-03-04 15:03:19
 */
public interface TbUserDao {


    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    /**
     * 更新
     * @param tbUserDTO
     * @return
     */
    int update(TbUserDTO tbUserDTO);

}