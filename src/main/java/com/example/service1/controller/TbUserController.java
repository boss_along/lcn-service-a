package com.example.service1.controller;

import com.example.service1.common.model.Result;
import com.example.service1.entity.TbUserDTO;
import com.example.service1.service.TbUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
/**
 * (TbUser)表控制层
 *
 * @author makejava
 * @since 2019-02-16 07:39:54
 */
@Controller
@RequestMapping("tbUser")
public class TbUserController {
    /**
     * 服务对象
     */
    @Resource
    private TbUserService tbUserService;

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/del")
    @ResponseBody
    public Result del(@RequestParam("id") String id,@RequestParam(value = "ex", required = false) String exFlag){
        tbUserService.deleteById(id,exFlag);
        return Result.ok();
    }


    @RequestMapping("/update")
    @ResponseBody
    public Result update(@RequestBody TbUserDTO tbUserDTO, @RequestParam(value = "ex", required = false) String exFlag){
        tbUserService.update(tbUserDTO);
        return Result.ok();
    }
}