package com.example.service1.service;

import com.example.service1.entity.TbUserDTO;

import java.util.List;

/**
 * (TbUser)表服务接口
 *
 * @author makejava
 * @since 2019-02-16 07:39:54
 */
public interface TbUserService {


    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id,String exFlag);

    /**
     *
     * @param tbUserDTO
     */
    void update(TbUserDTO tbUserDTO);

}