package com.example.service1.service.impl;

import com.codingapi.tx.annotation.TxTransaction;
import com.example.service1.common.model.Result;
import com.example.service1.dao.TbUserDao;
import com.example.service1.entity.TbUserDTO;
import com.example.service1.service.TbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * (TbUser)表服务实现类
 *
 * @author makejava
 * @since 2019-02-16 07:39:54
 */
@Service("tbUserService")
public class TbUserServiceImpl implements TbUserService {
    @Resource
    private TbUserDao tbUserDao;

    @Autowired
    private com.example.service1.client.TbUserService userService;


    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @TxTransaction(isStart = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteById(String id,String exFlag) {
        int i = tbUserDao.deleteById(id);
        Result del = userService.del("2");
        int a = 1/0;
        return  true;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(TbUserDTO tbUserDTO) {
        tbUserDao.update(tbUserDTO);
        userService.update(tbUserDTO);
        tbUserDao.update(tbUserDTO);
    }
}