package com.example.service1.client;

import com.example.service1.common.model.Result;
import com.example.service1.entity.TbUserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "service-hi2",fallback = TbUserServiceImpl.class)
public interface TbUserService {

    @RequestMapping(value = "/tbUser/del")
    public Result del(String id);

    @RequestMapping(value = "/tbUser/update")
    public Result update(@RequestBody TbUserDTO userDTO);
}

class TbUserServiceImpl implements TbUserService{

    @Override
    public Result del(String id) {
        return Result.error(10001,"请稍后重试");
    }

    @Override
    public Result update(TbUserDTO userDTO) {
        return Result.error(10001,"请稍后重试");
    }
}
