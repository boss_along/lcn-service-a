package com.example.service1.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PageModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9062379354739912845L;
	
	private String code = "";
	
	private String msg = "";
	
	private long count = 0l;
	
	private List<?> data = new ArrayList<>();
	
	

	public PageModel() {
		super();
	}

	public PageModel(long count, List<?> data) {
		super();
		this.count = count;
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}
		
}
