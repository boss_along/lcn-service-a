package com.example.service1.common.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author:
 * @Date: 2019/1/16 18:37
 * @Description:
 */
@Data
public class Result implements Serializable {

    private boolean success;

    private Integer errcode;

    private String errmsg;

    private Object data;

    private Result(boolean success, Integer errcode, String errmsg, Object data){
        this.success = success;
        this.errcode = errcode;
        this.errmsg = errmsg;
        this.data = data;
    }

    /**
     * 有结果集
     * @param success 是否成功
     * @param errcode 返回码
     * @param errmsg 返回信息
     * @param data 返回数据
     * @return
     */
    public static Result ok(boolean success, Integer errcode, String errmsg, Object data){
        return new Result(success,errcode,errmsg,data);
    }

    /**
     * 有返回数据集
     * @param data 返回数据集
     * @return
     */
    public static Result ok(Object data){
       return ok(true,200,null,data);
    }

    /**
     * 无结果集
     * @param errcode 返回码
     * @param errmsg 返回信息
     * @return
     */
    public static Result ok(Integer errcode,String errmsg){
        return ok(true,errcode,errmsg,null);
    }

    /**
     * 无返回数据集
     * @return
     */
    public static Result ok(){
        return ok(true,200,null,null);
    }

    /**
     * 错误
     * @param errcode 错误码
     * @param errmesg 错误信息
     * @return
     */
    public static Result error(Integer errcode,String errmesg){
        return error(errcode,errmesg,null);
    }

    /**
     * 错误
     * @param errcode 错误码
     * @param errmesg 错误信息
     * @param data 错误数据
     * @return
     */
    public static Result error(Integer errcode,String errmesg,Object data){
        return error(false,errcode,errmesg,data);
    }

    /**
     * 错误
     * @param success
     * @param errcode
     * @param errmsg
     * @param data
     * @return
     */
    public static Result error(boolean success, Integer errcode, String errmsg, Object data){
        return new Result(success,errcode,errmsg,data);
    }
}
