package com.example.service1.common.model;

import lombok.Data;

import java.util.Objects;

/**
 * @author Thinkpad
 * @Title: ErrorPosition
 * @ProjectName work
 * @Description: TODO
 * @date 2019/3/1913:53
 * @Version: 1.0
 */
@Data
public class ErrorPosition {

    private Integer row;

    private Integer column;

    private String errMsg;


    public ErrorPosition() {}

    public ErrorPosition(Integer row, Integer column, String errMsg) {
        this.row = row;
        this.column = column;
        this.errMsg = errMsg;
    }

    public ErrorPosition(String errMsg) {
        this(null, null, errMsg);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        ErrorPosition that = (ErrorPosition) o;
        return Objects.equals(row, that.row) &&
                Objects.equals(column, that.column) &&
                Objects.equals(errMsg, that.errMsg);
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, column, errMsg);
    }
}
