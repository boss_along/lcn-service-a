package com.example.service1.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * (TbUser)实体类
 *
 * @author makejava
 * @since 2019-03-04 16:35:38
 */
@Data
public class TbUserDTO implements Serializable {
    
    private static final long serialVersionUID = -34627837454760896L;    
            
    /**
    * ID    
    */    
    private String id;   
            
    /**
    * 用户名    
    */    
    private String username;   
            
    /**
    * 密码    
    */    
    private String password;   
            
    /**
    * 角色    
    */    
    private String role;   
            
    /**
    * 手机号    
    */    
    private String phone;   
            
    /**
    * 昵称    
    */    
    private String nick;   
            
    /**
    * 头像    
    */    
    private String pic;   
            
    /**
    * 创建人    
    */    
    private String creater;   
            
    /**
    * 创建时间    
    */    
    private Date createTime;   
            
    /**
    * 修改人    
    */    
    private String modifier;   
            
    /**
    * 修改时间    
    */    
    private Date modifyTime;   
            
    /**
    * 状态    
    */    
    private String status;   
        
}